const dummy_user = {
  id: 1,
  email: "test@test.com",
  password: "123456",
};

class Service {
  signin(email, password) {
    return new Promise((resolve, reject) => {
      // authenticate user
      if (email === dummy_user.email && password === dummy_user.password) {
        resolve({ ...dummy_user, password: undefined });
      }

      reject(new Error("Incorrect credentials"));
    });
  }

  getUsernameSuggestions(search) {
    return fetch(
      `https://vortex.worldofwarships.ru/api/accounts/search/autocomplete/${search}`
    )
      .then((res) => res.json())
      .then((response) => {
        if (response.status === "ok") {
          return response.data.map((i) => ({ label: i.name, value: i.spa_id }));
        }
        return [];
      });
  }

  getUserList(search) {
    return fetch(
      `https://vortex.worldofwarships.ru/api/accounts/search/${search}`
    )
      .then((res) => res.json())
      .then((response) => {
        if (response.status === "ok") {        
          return response.data.map((i) => {
            const {pve = {}, pvp = {}} = i.statistics || {}
            const battl = pve.battles_count + pvp.battles_count || 0
            const win = (pve.wins + pvp.wins)/battl * 100 || 0
           return { 
            value: i.spa_id,
            name: i.name,
            battles: battl,
            wins: win.toFixed(2) + '%',
          }

          });
        }
        return [];
      });
  }

}

export default new Service();