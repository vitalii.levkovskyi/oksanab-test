import service from "../service";
import router from '../router';

const savedUserJson = localStorage.getItem("user");

export default {
  namespaced: true,
  state: {
    user: savedUserJson ? JSON.parse(savedUserJson) : null,
  },
  mutations: {
    signin(state, user) {
      state.user = user;
    },
    signout(state) {
      state.user = null;
    },
  },
  actions: {
    signin(ctx, user) {
      console.log(user);
      localStorage.setItem("user", JSON.stringify(user));
      ctx.commit("signin", user)
    },
    signout(ctx) {
      localStorage.removeItem("user");
      if (router.currentRoute.meta.requiresAuth) {
        router.push('login')
      }
      ctx.commit("signout")
    }
  },
  getters: {
    isAuthenticated(state) {
      return !!state.user;
    },
  },
};
